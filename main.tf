###############################################################################
# GKE Cluster Configuration Module
# -----------------------------------------------------------------------------
# main.tf
###############################################################################

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 3.47"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = ">= 3.47"
    }
  }
  required_version = ">= 0.13"
}

# Get the list of current supported Kubernetes versions
data "google_container_engine_versions" "version" {
  provider       = google-beta
  location       = var.gke_cluster_type == "regional" ? var.gcp_region : var.gcp_region_zone
  version_prefix = var.gke_version_prefix
}

# Create subnet for GKE cluster using variable CIDR range
resource "google_compute_subnetwork" "subnet" {
  description              = "Subnet for ${var.description}"
  name                     = "${var.cluster_name}-subnet-nodes"
  network                  = var.gcp_network
  private_ip_google_access = "true"
  region                   = var.gcp_region

  # CIDR Subnet
  # ---------------------------------------------------------------------------
  # 10.130.0.0/16
  # 10.130.0.1 - 10.130.255.254
  #
  # 10.135.0.0/20
  # 10.135.0.1 - 10.135.15.254
  #
  # 10.140.0.0/22
  # 10.140.0.1 - 10.140.3.254

  # Node IPs
  # ---------------------------------------------------------------------------
  # cidrsubnet("10.130.0.0/16", 4, 0) => 10.130.0.0/20
  # 10.130.0.1 to 10.130.15.255 (~2,048 usable IPs with Kubernetes 50% utilization)
  #
  # cidrsubnet("10.135.0.0/20", 4, 0) => 10.135.0.0/24
  # 10.135.0.1 to 10.135.0.254 (~128 usable IPs with Kubernetes 50% utilization)
  #
  # cidrsubnet("10.140.0.0/22", 4, 0) => 10.140.0.0/26
  # 10.140.0.1 to 10.140.0.62 (~30 usable IPs with Kubernetes 50% utilization)

  ip_cidr_range = cidrsubnet(var.gke_cluster_cidr, 4, 0)

  # Services IPs
  # ---------------------------------------------------------------------------
  # cidrsubnet("10.130.0.0/16", 2, 1) => 10.130.64.0/18
  # 10.130.64.1 to 10.130.127.254 (~8,192 usable IPs with Kubernetes 50% utilization)
  #
  # cidrsubnet("10.135.0.0/20", 2, 1) => 10.135.4.0/22
  # 10.135.4.1 - 10.135.7.254 (~512 usable IPs with Kubernetes 50% utilization)
  #
  # cidrsubnet("10.140.0.0/22", 2, 1) => 10.140.1.0/24
  # 10.140.1.1 - 10.140.1.254 (~128 usable IPs with Kubernetes 50% utilization)

  secondary_ip_range {
    range_name    = "${var.cluster_name}-subnet-services"
    ip_cidr_range = cidrsubnet(var.gke_cluster_cidr, 2, 1)
  }

  # Pod IPs
  # ---------------------------------------------------------------------------
  # cidrsubnet("10.130.0.0/16", 1, 1) => 10.130.128.0/17
  # 10.130.128.1 - 10.130.255.254 (~16,383 usable IPs with Kubernetes 50% utilization)
  #
  # cidrsubnet("10.135.0.0/20", 1, 1) => 10.135.8.0/21
  # 10.135.8.1 - 10.135.15.254 (~1,024 usable IPs with Kubernetes 50% utilization)
  #
  # cidrsubnet("10.140.0.0/22", 1, 1) => 10.140.2.0/23
  # 10.140.2.1 - 10.140.3.254 (~256 usable IPs with Kubernetes 50% utilization)

  secondary_ip_range {
    range_name    = "${var.cluster_name}-subnet-pods"
    ip_cidr_range = cidrsubnet(var.gke_cluster_cidr, 1, 1)
  }

}

# Create Kubernetes cluster on GKE
resource "google_container_cluster" "cluster" {
  description        = var.description
  location           = var.gke_cluster_type == "regional" ? var.gcp_region : var.gcp_region_zone
  min_master_version = data.google_container_engine_versions.version.release_channel_default_version[var.gke_release_channel]
  name               = var.cluster_name
  network            = var.gcp_network
  provider           = google-beta
  resource_labels    = var.labels

  # Node IPs
  subnetwork = google_compute_subnetwork.subnet.self_link

  ip_allocation_policy {
    # Pod IPs
    cluster_secondary_range_name = "${var.cluster_name}-subnet-pods"
    # Services IPs
    services_secondary_range_name = "${var.cluster_name}-subnet-services"
  }

  # We can't create a cluster without a node pool defined, however we only want
  # to use Terraform-managed node pools. We will create the smallest possible
  # default node pool and immediately delete it.
  remove_default_node_pool  = true
  initial_node_count        = 1
  default_max_pods_per_node = var.gke_node_pods_count_max

  maintenance_policy {
    daily_maintenance_window {
      start_time = var.gke_maintenance_start_time
    }
  }

  # If this block is provided and both username and password are empty, basic authentication will be disabled.
  master_auth {
    client_certificate_config {
      issue_client_certificate = false
    }
  }

  # As of 2020-05-22, this is only available in the `google-beta` provider. If
  # using the `google` provider, change at the top of the `environments/*` file.
  # Beta GCP features have no deprecation policy and no SLA, but are otherwise
  # considered to be feature-complete with only minor outstanding issues.
  # https://www.terraform.io/docs/providers/google/guides/provider_versions.html
  release_channel {
    channel = var.gke_release_channel
  }

  addons_config {
    horizontal_pod_autoscaling {
      disabled = "false"
    }
  }


  dynamic "workload_identity_config" {
    for_each = var.workload_identity_enabled ? [1] : []
    content {
      workload_pool = "${var.gcp_project}.svc.id.goog"
    }
  }

  dynamic "workload_identity_config" {
    for_each = var.workload_identity_enabled ? [] : [1]
    content {
      workload_pool = null
    }
  }

  # Cluster auto scaling supports multiple approaches. The "enabled" key is for
  # Node Auto Provisioning and is not required for cluster autoscaling. If
  # enabled is true, then resource limits need to be set.
  # https://cloud.google.com/kubernetes-engine/docs/how-to/node-auto-provisioning
  #
  # For most use cases, the autoscaling_profile is all that is needed.
  # https://cloud.google.com/kubernetes-engine/docs/concepts/cluster-autoscaler#autoscaling_profiles
  cluster_autoscaling {
    enabled             = "false"
    autoscaling_profile = var.gke_autoscaling_profile
    /*
    resource_limits {
      resource_type = "cpu"
      minimum       = ""
      maximum       = ""
    }
    resource_limits {
      resource_type = "memory"
      minimum       = ""
      maximum       = ""
    }
    */
  }
}

# Create an independently managed node pool for the cluster
resource "google_container_node_pool" "node_pool" {
  cluster            = google_container_cluster.cluster.name
  initial_node_count = var.gke_node_count_min
  location           = var.gke_cluster_type == "regional" ? var.gcp_region : var.gcp_region_zone
  name               = "${var.cluster_name}-node-pool"
  version            = data.google_container_engine_versions.version.release_channel_default_version[var.gke_release_channel]

  lifecycle {
    ignore_changes = [initial_node_count]
  }

  autoscaling {
    min_node_count = var.gke_node_count_min
    max_node_count = var.gke_node_count_max
  }

  management {
    auto_repair  = true
    auto_upgrade = true
  }

  node_config {
    labels       = var.labels
    preemptible  = var.gcp_preemptible_nodes
    machine_type = var.gcp_machine_type

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}
