###############################################################################
# GKE Cluster Configuration Module
# -----------------------------------------------------------------------------
# variables.tf
###############################################################################

# Required variables

variable "cluster_name" {
  type        = string
  description = "Cluster short name (Ex. cool-product-cluster). Only alphanumeric characters and hyphens are allowed. The name will be suffixed with `-cluster` and `-nodes`, `-services`, and `-pods` for each respective IP range."
}

variable "description" {
  type        = string
  description = "Cluster description (Ex. My cluster for cool product containers)"
}

variable "gcp_network" {
  type        = string
  description = "The object or self link for the network created in the parent module (Ex. google_compute_network.sandbox_vpc.self_link)"
}

variable "gcp_project" {
  type        = string
  description = "The GCP project ID (may be an alphanumeric slug) that the resources are deployed in. (Example: my-project-name)"
}

variable "gcp_region" {
  type        = string
  description = "The GCP region name for this environment (Ex. us-east1)"
}

variable "gcp_region_zone" {
  type        = string
  description = "The GCP region availability zone for this environment. This must match the gcp_region. (Ex. us-east1-c)"
}

variable "gke_cluster_cidr" {
  type        = string
  description = "The network CIDR range (Ex. 10.x.y.0/22, 10.x.y.0/20, 10.x.0.0/16) for this cluster. See the README for more information on Kubernetes cluster subnet numbering and size recommendations."
}

# Optional variables with default values

variable "gcp_machine_type" {
  type        = string
  description = "The GCP machine type to use for cluster nodes. See the README for instructions on custom cluster sizing. In most cases, you should leave this at the default value. (Default: e2-standard-2)"
  default     = "e2-standard-2"
}

variable "gcp_preemptible_nodes" {
  type        = bool
  description = "Preemptible VMs are Compute Engine VM instances that last a maximum of 24 hours and provide no availability guarantees. You can use preemptible VMs in your GKE clusters or node pools to run batch or fault-tolerant jobs that are less sensitive to the ephemeral, non-guaranteed nature of preemptible VMs. For better availability, use smaller machine types. We set this to true for sandbox use cases for cost savings, and false for production use cases for reliability. https://cloud.google.com/compute/docs/instances/preemptible#preemption_process (Default: true)"
  default     = "true"
}

variable "gke_autoscaling_profile" {
  type        = string
  description = "The autoscaling profile for cluster nodes (Ex. BALANCED|OPTIMIZE_UTILIZATION)"
  default     = "BALANCED"
}

variable "gke_cluster_type" {
  type        = string
  description = "If gcp_cluster_type is `regional`, the cluster will be across 3 zones in the region. This is triple redundancy that costs 3x more than a zonal cluster. If gcp_cluster_type is `zonal`, the cluster will be created with 1 zone and is more cost efficient for sandbox purposes."
  default     = "zonal"
}

variable "gke_node_count_max" {
  type        = number
  description = "The maximum number of nodes in the autoscaling node pool that should be running at any given time. This must be greater than 1. See the README for instructions on cluster sizing. If gke_cluster_type is `regional`, the max_node_count should be divided by 3 and rounded down to the nearest whole number, since this is calculated based on the number of nodes running in each of the 3 availability zones."
  default     = 8
}

variable "gke_node_count_min" {
  type        = number
  description = "The minimum number of nodes in the autoscaling node pool that should be running at any given time. This must be greater than 0 and less than local.max_node_count. If gcp_cluster_type is `regional`, a value of `1` will be interpreted as `3` (one node in 3 separate availability zones). (Default: 1)"
  default     = 1
}

variable "gke_node_pods_count_max" {
  type        = number
  description = "The maximum number of pods per node. See the README for instructions on cluster sizing."
  default     = 32
}

variable "gke_maintenance_start_time" {
  type        = string
  description = "The start time of maintenance windows for Kubernetes operations and upgrades in HH:MM format (GMT) timezone. (Default: 03:00)"
  default     = "03:00"
}

variable "gke_release_channel" {
  type        = string
  description = "The release channel for the cluster. (Ex. UNSPECIFIED|STABLE|REGULAR|RAPID)."
  default     = "REGULAR"
}

variable "gke_version_prefix" {
  type        = string
  description = "The version number for the Kubernetes master and nodes. This is fuzz tested using the google_container_engine_versions data source to get the appropriate version number. (Ex. 1.18.)."
  default     = "1.18."
}

variable "labels" {
  description = "Labels to place on the cluster and node pool resources."
  type        = map(any)
  default     = {}
}

variable "workload_identity_enabled" {
  description = "Enables or Disables Google Workload Identity on the cluster."
  type = bool
  default = false
}
